| Legajo     | Alumno     | Grupo      | 
| ---------- | ---------  | ---------  | 
|  3740   | ALVAREZ, MARIANO EZEQUIEL  | [1](https://gitlab.com/so-unq-2019-s1/grupo_1)       | 
| 30465   | BADA, MIGUEL ENRIQUE  |       | 
| 33108   | BARRAZA, MIGUEL ANGEL  |       | 
| 25947   | BARREIRO, GUSTAVO EMILIANO  |       | 
| 17568   | CANUHÉ, LAURA MABEL  |       | 
| 40315   | CARDOZO, ADRIAN DAVID  |       | 
| 20615   | CIONCI, MATIAS  |       | 
| 39974   | EQUILEA, MIGUEL ANGEL  |       | 
| 21768   | ESCOBAR, GRACIELA GISELE  |       | 
| 31873   | ESPINOLA, ABEL ENRIQUE  |       | 
| 34919   | GALARZA, ENZO NAHUEL  |       | 
| 36558   | GATTONE, MARIA DE LOS ANGELES  |       | 
| 33341   | GIUNIANETTI, JAVIER EDUARDO  |       | 
| 23130   | GONZALEZ, OCTAVIO  |       | 
| 39800   | IGLESIAS, NAHUEL  |       | 
| 33405   | LATTANZIO, BRUNO JOAQUIN  |       | 
| 26348   | LEIVA, NESTOR LEANDRO  |       | 
| 31435   | LOPEZ, SUSANA MARIEL  |       | 
| 29722   | MAIA, JONATHAN NICOLAS  |       | 
| 34171   | MAIDANA, MAXIMILIANO GABRIEL  |       | 
| 43779   | MALSAM, LEANDRO MARTIN  |       | 
| 35327   | MAMANI JATABE, LUIS ALEJANDRO  |       | 
| 43541   | MARTINEZ LOPEZ, LAUTARO GASTON  |       | 
| 35380   | MATTERA, LUCAS IVAN  |       | 
| 36338   | MESIANO, CRISTIAN GABRIEL  |       | 
| 33501   | MUÑOZ, FERNANDO NICOLAS  |       | 
| 33684   | SALDAÑA, CARLOS JOEL  |       | 
| 35849   | SANDOVAL, ELIAS DANIEL  |       | 
| 32342   | SARDI, FACUNDO  |       | 
| 28708   | SOÑEZ, MAILIN  |       | 
| 33772   | VELAZQUEZ, MARIANA ANABEL  |       | 
| --   | DOMINGUEZ ROZO, ANDRES DAVID  |       | 


Total: 32 Alumnos